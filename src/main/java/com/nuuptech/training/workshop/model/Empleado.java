package com.nuuptech.training.workshop.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Empleado implements Serializable {

    @Id
    @GeneratedValue(generator = "empleado_id_seq")
    @SequenceGenerator(name = "empleado_id_seq", sequenceName = "empleado_id_seq", allocationSize = 1)
    @JsonIgnore
    private Long id;

    @ManyToOne
    @JoinColumn
    @NotNull
    private Departamento departamento;

    @Column
    @NotNull
    private String nombre;

    @Column
    @NotNull
    private String apellidoPaterno;

    @Column
    private String apellidoMaterno;

    @Column
    @NotNull
    private Date fechaDeIngreso;

    @Column
    @NotNull
    private String clave;

}
