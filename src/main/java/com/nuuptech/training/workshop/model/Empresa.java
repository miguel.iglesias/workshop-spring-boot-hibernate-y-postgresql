package com.nuuptech.training.workshop.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Empresa implements Serializable {

    @Id
    @GeneratedValue(generator = "empresa_id_seq")
    @SequenceGenerator(name = "empresa_id_seq", sequenceName = "empresa_id_seq", allocationSize = 1)
    @JsonIgnore
    private Long id;

    @Column
    @NotNull
    private String razonSocial;

    @Column
    @NotNull
    private String nombreComercial;

    @Column
    @NotNull
    private Date fechaDeFundacion;

    @Column
    @NotNull
    private String Direccion;

    @Column
    @NotNull
    private Boolean activa;

    @Column
    private String slogan;

}
