package com.nuuptech.training.workshop.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Departamento implements Serializable {

    @Id
    @GeneratedValue(generator = "departamento_id_seq")
    @SequenceGenerator(name = "departamento_id_seq", sequenceName = "departamento_id_seq", allocationSize = 1)
    @JsonIgnore
    private Long id;

    @ManyToOne
    @JoinColumn
    @NotNull
    private Empresa empresa;

    @Column
    @NotNull
    private String nombreDelDepartamento;

    @Column
    private String clave;

    @Column
    @NotNull
    private Date fechaDeCreacion;

}
