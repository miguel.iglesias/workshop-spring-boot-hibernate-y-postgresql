package com.nuuptech.training.workshop.repository;

import com.nuuptech.training.workshop.model.Departamento;
import com.nuuptech.training.workshop.model.Empleado;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.QueryHint;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.hibernate.jpa.QueryHints.HINT_FETCH_SIZE;

@Repository
public interface EmpleadoRepository extends JpaRepository<Empleado, Long> {

    @Query("SELECT e FROM Empleado e WHERE e.apellidoPaterno LIKE %:apellidoPaterno%")
    List<Empleado> finAllByApellidoPaterno(@Param("apellidoPaterno") String apellidoPaterno);

    @Query("SELECT e FROM Empleado e WHERE e.clave = :clave")
    Optional<Empleado> findByClave(@Param("clave") String clave);

    @Query("SELECT e FROM Empleado e WHERE e.departamento = :departamento")
    List<Empleado> findAllByDepartamento(@Param("departamento") Departamento departamento);

    @Query("SELECT e FROM Empleado e")
    Page<Empleado> findAll(Pageable pageable);

    @Query("SELECT e FROM Empleado e")
    @QueryHints(value = @QueryHint(name = HINT_FETCH_SIZE, value = "100"))
    Stream<Empleado> streamAll();

}
