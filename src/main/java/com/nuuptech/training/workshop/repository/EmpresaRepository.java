package com.nuuptech.training.workshop.repository;

import com.nuuptech.training.workshop.model.Empresa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.QueryHint;
import java.util.List;
import java.util.stream.Stream;

import static org.hibernate.jpa.QueryHints.HINT_FETCH_SIZE;

@Repository
public interface EmpresaRepository extends JpaRepository<Empresa, Long> {

    @Query("SELECT e FROM Empresa e WHERE e.razonSocial LIKE %:razonSocial%")
    List<Empresa> findAllByRazonSocial(@Param("razonSocial") String razonSocial);

    @Query("SELECT e FROM Empresa e WHERE e.nombreComercial LIKE %:nombreComercial%")
    List<Empresa> findAllByNombreComercial(@Param("nombreComercial") String nombreComercial);

    @Query("SELECT e FROM Empresa e")
    Page<Empresa> findAll(Pageable pageable);

    @Query("SELECT e FROM Empresa e")
    @QueryHints(value = @QueryHint(name = HINT_FETCH_SIZE, value = "100"))
    Stream<Empresa> streamAll();

}
