package com.nuuptech.training.workshop.repository;

import com.nuuptech.training.workshop.model.Departamento;
import com.nuuptech.training.workshop.model.Empresa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.QueryHint;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.hibernate.jpa.QueryHints.HINT_FETCH_SIZE;

@Repository
public interface DepartamentoRepository extends JpaRepository<Departamento, Long> {

    @Query("SELECT d FROM Departamento d WHERE d.clave = :clave")
    Optional<Departamento> findByClave(@Param("clave") String clave);

    @Query("SELECT d FROM Departamento d WHERE d.empresa = :empresa")
    List<Departamento> findAllByEmpresa(@Param("empresa") Empresa empresa);

    @Query("SELECT d FROM Departamento d")
    Page<Departamento> findAll(Pageable pageable);

    @Query("SELECT d FROM Departamento d")
    @QueryHints(value = @QueryHint(name = HINT_FETCH_SIZE, value = "100"))
    Stream<Departamento> streamAll();

}
