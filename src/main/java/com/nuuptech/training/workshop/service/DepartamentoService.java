package com.nuuptech.training.workshop.service;

import com.nuuptech.training.workshop.model.Departamento;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.stream.Stream;

public interface DepartamentoService {

    public Departamento save(Departamento departamento);

    public Departamento findById(Long id);

    public Departamento findByClave(String clave);

    public List<Departamento> findAllByEmpresa(Long idEmpresa);

    public Page<Departamento> findAll(Pageable pageable);

    public Stream<Departamento> streamAll();

    public boolean deleteById(Long id);
}
