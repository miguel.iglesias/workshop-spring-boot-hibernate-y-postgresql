package com.nuuptech.training.workshop.service.impl;

import com.nuuptech.training.workshop.exception.EntityNotFoundException;
import com.nuuptech.training.workshop.model.Empresa;
import com.nuuptech.training.workshop.repository.EmpresaRepository;
import com.nuuptech.training.workshop.service.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Stream;

@Transactional
@Service
public class EmpresaServiceImpl implements EmpresaService {

    @Autowired
    private EmpresaRepository empresaRepository;

    @Override
    public Empresa save(Empresa empresa) {
        return empresaRepository.save(empresa);
    }

    @Override
    public Empresa findById(Long id) {
        return empresaRepository.findById(id)
                .orElseThrow(() -> EntityNotFoundException
                        .from("No existe la empresa con id ", id.toString()));
    }

    @Override
    public List<Empresa> findAllByRazonSocial(String razonSocial) {
        return empresaRepository.findAllByRazonSocial(razonSocial);
    }

    @Override
    public List<Empresa> findAllByNombreComercial(String nombreComercial) {
        return empresaRepository.findAllByNombreComercial(nombreComercial);
    }

    @Override
    public Page<Empresa> findAll(Pageable pageable) {
        return empresaRepository.findAll(pageable);
    }

    @Override
    public Stream<Empresa> streamAll() {
        return empresaRepository.streamAll();
    }

    @Override
    public boolean deleteById(Long id) {
        empresaRepository.deleteById(id);
        return true;
    }

}
