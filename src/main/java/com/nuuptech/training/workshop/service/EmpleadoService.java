package com.nuuptech.training.workshop.service;

import com.nuuptech.training.workshop.model.Empleado;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.stream.Stream;

public interface EmpleadoService {

    public Empleado save(Empleado empleado);

    public Empleado findById(Long id);

    public Empleado findByClave(String clave);

    public List<Empleado> finAllByApellidoPaterno(String apellidoPaterno);

    public List<Empleado> findAllByDepartamento(Long idDepartamento);

    public Page<Empleado> findAll(Pageable pageable);

    public Stream<Empleado> streamAll();

    public boolean deleteById(Long id);
}
