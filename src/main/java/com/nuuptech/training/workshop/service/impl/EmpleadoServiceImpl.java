package com.nuuptech.training.workshop.service.impl;

import com.nuuptech.training.workshop.exception.EntityNotFoundException;
import com.nuuptech.training.workshop.model.Empleado;
import com.nuuptech.training.workshop.repository.EmpleadoRepository;
import com.nuuptech.training.workshop.service.DepartamentoService;
import com.nuuptech.training.workshop.service.EmpleadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Stream;

@Transactional
@Service
public class EmpleadoServiceImpl implements EmpleadoService {

    @Autowired
    private DepartamentoService departamentoService;

    @Autowired
    private EmpleadoRepository empleadoRepository;

    @Override
    public Empleado save(Empleado empleado) {
        return empleadoRepository.save(empleado);
    }

    @Override
    public Empleado findById(Long id) {
        return empleadoRepository.findById(id)
                .orElseThrow(() -> EntityNotFoundException
                        .from("No existe el empleado con id ", id.toString()));
    }

    @Override
    public Empleado findByClave(String clave) {
        return empleadoRepository.findByClave(clave)
                .orElseThrow(() -> EntityNotFoundException
                        .from("No existe el empleado con clave ", clave));
    }

    @Override
    public List<Empleado> finAllByApellidoPaterno(String apellidoPaterno) {
        return empleadoRepository.finAllByApellidoPaterno(apellidoPaterno);
    }

    @Override
    public List<Empleado> findAllByDepartamento(Long idDepartamento) {
        return empleadoRepository.findAllByDepartamento(departamentoService.findById(idDepartamento));
    }

    @Override
    public Page<Empleado> findAll(Pageable pageable) {
        return empleadoRepository.findAll(pageable);
    }

    @Override
    public Stream<Empleado> streamAll() {
        return empleadoRepository.streamAll();
    }

    @Override
    public boolean deleteById(Long id) {
        empleadoRepository.deleteById(id);
        return true;
    }
}
