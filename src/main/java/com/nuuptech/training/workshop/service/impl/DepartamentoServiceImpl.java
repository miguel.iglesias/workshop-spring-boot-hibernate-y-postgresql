package com.nuuptech.training.workshop.service.impl;

import com.nuuptech.training.workshop.exception.EntityNotFoundException;
import com.nuuptech.training.workshop.model.Departamento;
import com.nuuptech.training.workshop.repository.DepartamentoRepository;
import com.nuuptech.training.workshop.service.DepartamentoService;
import com.nuuptech.training.workshop.service.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Stream;

@Transactional
@Service
public class DepartamentoServiceImpl implements DepartamentoService {

    @Autowired
    private DepartamentoRepository departamentoRepository;

    @Autowired
    private EmpresaService empresaService;

    @Override
    public Departamento save(Departamento departamento) {
        return departamentoRepository.save(departamento);
    }

    @Override
    public Departamento findById(Long id) {
        return departamentoRepository.findById(id)
                .orElseThrow(() -> EntityNotFoundException
                        .from("No existe el departamento con id ", id.toString()));
    }

    @Override
    public Departamento findByClave(String clave) {
        return departamentoRepository.findByClave(clave)
                .orElseThrow(() -> EntityNotFoundException
                        .from("No existe el departamento con clave ", clave));
    }

    @Override
    public List<Departamento> findAllByEmpresa(Long idEmpresa) {
        return departamentoRepository.findAllByEmpresa(empresaService.findById(idEmpresa));
    }

    @Override
    public Page<Departamento> findAll(Pageable pageable) {
        return departamentoRepository.findAll(pageable);
    }

    @Override
    public Stream<Departamento> streamAll() {
        return departamentoRepository.streamAll();
    }

    @Override
    public boolean deleteById(Long id) {
        departamentoRepository.deleteById(id);
        return true;
    }

}
