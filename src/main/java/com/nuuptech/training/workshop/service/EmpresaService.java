package com.nuuptech.training.workshop.service;

import com.nuuptech.training.workshop.model.Empresa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.stream.Stream;

public interface EmpresaService {

    public Empresa save(Empresa empresa);

    public Empresa findById(Long id);

    public List<Empresa> findAllByRazonSocial(String razonSocial);

    public List<Empresa> findAllByNombreComercial(String nombreComercial);

    public boolean deleteById(Long id);

    public Page<Empresa> findAll(Pageable pageable);

    public Stream<Empresa> streamAll();

}
