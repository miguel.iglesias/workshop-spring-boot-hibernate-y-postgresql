package com.nuuptech.training.workshop.controller;

import com.nuuptech.training.workshop.model.Departamento;
import com.nuuptech.training.workshop.model.Empresa;
import com.nuuptech.training.workshop.service.DepartamentoService;
import com.nuuptech.training.workshop.service.EmpresaService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Log
@CrossOrigin(origins = "*", maxAge = 3600L)
@RestController
@RequestMapping(path = "/empresa")
public class EmpresaController {

    @Autowired
    private DepartamentoService departamentoService;

    @Autowired
    private EmpresaService empresaService;

    @RequestMapping(value = "/",
            method = RequestMethod.GET,
            produces = {
                    MediaType.APPLICATION_JSON_VALUE
            })
    @ResponseBody
    public ResponseEntity<Page<Empresa>> findAll(@RequestParam(defaultValue = "0") int page,
                                                 @RequestParam(defaultValue = "10") int size) {
        return new ResponseEntity<>(empresaService.findAll(PageRequest.of(page, size)), new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/",
            method = RequestMethod.POST,
            consumes = {
                    MediaType.APPLICATION_JSON_VALUE
            },
            produces = {
                    MediaType.APPLICATION_JSON_VALUE
            })
    @ResponseBody
    public ResponseEntity<Empresa> create(@RequestBody Empresa empresa) {
        return new ResponseEntity<>(empresaService.save(empresa), new HttpHeaders(), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}",
            method = RequestMethod.GET,
            produces = {
                    MediaType.APPLICATION_JSON_VALUE
            })
    @ResponseBody
    public ResponseEntity<Empresa> findById(@PathVariable("id") Long id) {

        return new ResponseEntity<>(empresaService.findById(id), new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}/departamento",
            method = RequestMethod.GET,
            produces = {
                    MediaType.APPLICATION_JSON_VALUE
            })
    @ResponseBody
    public ResponseEntity<List<Departamento>> findDepartamentos(@PathVariable("id") Long id) {

        return new ResponseEntity<>(departamentoService.findAllByEmpresa(id), new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}",
            method = RequestMethod.DELETE,
            produces = {
                    MediaType.APPLICATION_JSON_VALUE
            })
    @ResponseBody
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {

        return new ResponseEntity<>(empresaService.deleteById(id), new HttpHeaders(), HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/_search/",
            method = RequestMethod.GET,
            produces = {
                    MediaType.APPLICATION_JSON_VALUE
            })
    @ResponseBody
    public ResponseEntity<List<Empresa>> search(@RequestParam(value = "razonSocial", required = false) String razonSocial,
                                                @RequestParam(value = "nombreComercial", required = false) String nombreComercial) {
        if(razonSocial != null) {
            return new ResponseEntity<>(empresaService.findAllByRazonSocial(razonSocial), new HttpHeaders(), HttpStatus.OK);
        } else if (nombreComercial != null) {
            return new ResponseEntity<>(empresaService.findAllByNombreComercial(nombreComercial), new HttpHeaders(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }
}
