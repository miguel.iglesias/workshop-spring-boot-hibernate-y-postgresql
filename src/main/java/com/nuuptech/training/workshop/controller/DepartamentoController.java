package com.nuuptech.training.workshop.controller;

import com.nuuptech.training.workshop.model.Departamento;
import com.nuuptech.training.workshop.model.Empleado;
import com.nuuptech.training.workshop.service.DepartamentoService;
import com.nuuptech.training.workshop.service.EmpleadoService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Log
@CrossOrigin(origins = "*", maxAge = 3600L)
@RestController
@RequestMapping(path = "/departamento")
public class DepartamentoController {

    @Autowired
    private DepartamentoService departamentoService;

    @Autowired
    private EmpleadoService empleadoService;

    @RequestMapping(value = "/",
            method = RequestMethod.GET,
            produces = {
                    MediaType.APPLICATION_JSON_VALUE
            })
    @ResponseBody
    public ResponseEntity<Page<Departamento>> findAll(@RequestParam(defaultValue = "0") int page,
                                                      @RequestParam(defaultValue = "10") int size) {
        return new ResponseEntity<>(departamentoService.findAll(PageRequest.of(page, size)), new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/",
            method = RequestMethod.POST,
            consumes = {
                    MediaType.APPLICATION_JSON_VALUE
            },
            produces = {
                    MediaType.APPLICATION_JSON_VALUE
            })
    @ResponseBody
    public ResponseEntity<Departamento> create(@RequestBody Departamento departamento) {
        return new ResponseEntity<>(departamentoService.save(departamento), new HttpHeaders(), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}",
            method = RequestMethod.GET,
            produces = {
                    MediaType.APPLICATION_JSON_VALUE
            })
    @ResponseBody
    public ResponseEntity<Departamento> findById(@PathVariable("id") Long id) {

        return new ResponseEntity<>(departamentoService.findById(id), new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}",
            method = RequestMethod.DELETE,
            produces = {
                    MediaType.APPLICATION_JSON_VALUE
            })
    @ResponseBody
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {

        return new ResponseEntity<>(departamentoService.deleteById(id), new HttpHeaders(), HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/{id}/empleado",
            method = RequestMethod.GET,
            produces = {
                    MediaType.APPLICATION_JSON_VALUE
            })
    @ResponseBody
    public ResponseEntity<List<Empleado>> findEmpledos(@PathVariable("id") Long id) {

        return new ResponseEntity<>(empleadoService.findAllByDepartamento(id), new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/_search/",
            method = RequestMethod.GET,
            produces = {
                    MediaType.APPLICATION_JSON_VALUE
            })
    @ResponseBody
    public ResponseEntity<List<Departamento>> search(@RequestParam(value = "clave", required = true) String clave) {
        return new ResponseEntity<>(new ArrayList() {{add(departamentoService.findByClave(clave));}}, new HttpHeaders(), HttpStatus.OK);
    }
}
