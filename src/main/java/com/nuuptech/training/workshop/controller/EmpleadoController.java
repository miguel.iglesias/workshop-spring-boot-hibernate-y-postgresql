package com.nuuptech.training.workshop.controller;

import com.nuuptech.training.workshop.model.Empleado;
import com.nuuptech.training.workshop.service.EmpleadoService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Log
@CrossOrigin(origins = "*", maxAge = 3600L)
@RestController
@RequestMapping(path = "/empleado")
public class EmpleadoController {

    @Autowired
    private EmpleadoService empleadoService;

    @RequestMapping(value = "/",
            method = RequestMethod.GET,
            produces = {
                    MediaType.APPLICATION_JSON_VALUE
            })
    @ResponseBody
    public ResponseEntity<Page<Empleado>> findAll(@RequestParam(defaultValue = "0") int page,
                                                  @RequestParam(defaultValue = "10") int size) {
        return new ResponseEntity<>(empleadoService.findAll(PageRequest.of(page, size)), new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/",
            method = RequestMethod.POST,
            consumes = {
                    MediaType.APPLICATION_JSON_VALUE
            },
            produces = {
                    MediaType.APPLICATION_JSON_VALUE
            })
    @ResponseBody
    public ResponseEntity<Empleado> create(@RequestBody Empleado empleado) {
        return new ResponseEntity<>(empleadoService.save(empleado), new HttpHeaders(), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}",
            method = RequestMethod.GET,
            produces = {
                    MediaType.APPLICATION_JSON_VALUE
            })
    @ResponseBody
    public ResponseEntity<Empleado> findById(@PathVariable("id") Long id) {

        return new ResponseEntity<>(empleadoService.findById(id), new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}",
            method = RequestMethod.DELETE,
            produces = {
                    MediaType.APPLICATION_JSON_VALUE
            })
    @ResponseBody
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {

        return new ResponseEntity<>(empleadoService.deleteById(id), new HttpHeaders(), HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/_search/",
            method = RequestMethod.GET,
            produces = {
                    MediaType.APPLICATION_JSON_VALUE
            })
    @ResponseBody
    public ResponseEntity<List<Empleado>> search(@RequestParam(value = "apellidoPaterno", required = false) String apellidoPaterno,
                                                @RequestParam(value = "clave", required = false) String clave) {
        if(apellidoPaterno != null) {
            return new ResponseEntity<>(empleadoService.finAllByApellidoPaterno(apellidoPaterno), new HttpHeaders(), HttpStatus.OK);
        } else if (clave != null) {
            Empleado empleado = empleadoService.findByClave(clave);
            return new ResponseEntity<>(new ArrayList() {{add(empleadoService.findByClave(clave));}}, new HttpHeaders(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }

}
